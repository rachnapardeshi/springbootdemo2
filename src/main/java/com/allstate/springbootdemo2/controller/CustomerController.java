package com.allstate.springbootdemo2.controller;

import com.allstate.springbootdemo2.entities.Address;
import com.allstate.springbootdemo2.entities.Customer;
import com.allstate.springbootdemo2.exception.OutOfRangeException;
import com.allstate.springbootdemo2.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/api/customer")
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @GetMapping("/status")
    public String status(){
        return "Customer Service Api is running";
    }

    @GetMapping("/all")
    public List<Customer> getAllCustomers(){
        return customerService.getAll();
    }

    @GetMapping("/find/{id}")
    public Optional<Customer> findById(@PathVariable int id) throws OutOfRangeException {
        return customerService.find(id);
    }

    @PostMapping("/save")
    public Customer save(@RequestBody Customer customer){
        return customerService.save(customer);
    }

    @PostMapping("/save1")
    public Customer save1(){
        Customer customer = new Customer(1,"Rachna",new Address("l1","l2","l3"));
        return customerService.save(customer);
    }
}
