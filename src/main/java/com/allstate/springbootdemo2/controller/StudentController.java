package com.allstate.springbootdemo2.controller;

import com.allstate.springbootdemo2.entities.Address;
import com.allstate.springbootdemo2.entities.Customer;
import com.allstate.springbootdemo2.entities.Student;
import com.allstate.springbootdemo2.service.StudentService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/api/student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping(value="/status")
    public String status(){
        return "Student Service Running";
    }

    @GetMapping(value="/all")
    public List<Student> getAllStudents(){
        return studentService.all();
    }

    @GetMapping(value="/find/{id}")
    public Optional<Student> findById(@PathVariable ObjectId id){
        return studentService.find(id);
    }

    @PostMapping(value="/save")
    public void save(@RequestBody Student student){
        studentService.save(student);
    }

    @PostMapping("/save1")
    public void save1(){
        Student student = new Student(null,1,"Rachna",new Address("l1","l2","l3"));
        studentService.save(student);
    }
}
