package com.allstate.springbootdemo2.service;

import com.allstate.springbootdemo2.entities.Student;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Optional;

public interface StudentService {
    List<Student> all();
    Optional<Student> find(ObjectId id) ;
    void save(Student student);
}
