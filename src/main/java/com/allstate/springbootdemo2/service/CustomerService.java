package com.allstate.springbootdemo2.service;

import com.allstate.springbootdemo2.entities.Customer;
import com.allstate.springbootdemo2.exception.OutOfRangeException;

import java.util.List;
import java.util.Optional;

public interface CustomerService {
    List<Customer> getAll();
    Optional<Customer> find(int id) throws OutOfRangeException;
    Customer save(Customer customer);
}
