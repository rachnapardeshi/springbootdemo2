package com.allstate.springbootdemo2.service;

import com.allstate.springbootdemo2.dao.StudentRepo;
import com.allstate.springbootdemo2.entities.Student;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService{

    @Autowired
    private StudentRepo studentRepo;

    @Override
    public List<Student> all() {
        return studentRepo.findAll();
    }

    @Override
    public Optional<Student> find(ObjectId id) {
        return studentRepo.findById(id);
    }

    @Override
    public void save(Student student) {
            studentRepo.save(student);
    }
}
