package com.allstate.springbootdemo2.service;

import com.allstate.springbootdemo2.dao.CustomerRepo;
import com.allstate.springbootdemo2.entities.Customer;
import com.allstate.springbootdemo2.exception.OutOfRangeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepo customerRepo;

    @Override
    public List<Customer> getAll() {
       return customerRepo.findAll();
    }

    @Override
    public Optional<Customer> find(int id) throws OutOfRangeException {
        if(id<1){
            throw new OutOfRangeException("Id must be over 0",1);
        }
        return customerRepo.findById(id);
    }

    @Override
    public Customer save(Customer customer) {
        return customerRepo.save(customer);
    }
}
