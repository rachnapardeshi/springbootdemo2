package com.allstate.springbootdemo2.dao;

import com.allstate.springbootdemo2.entities.Student;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StudentRepo extends MongoRepository<Student, ObjectId> {

}
