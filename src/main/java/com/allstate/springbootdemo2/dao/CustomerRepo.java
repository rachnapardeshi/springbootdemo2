package com.allstate.springbootdemo2.dao;

import com.allstate.springbootdemo2.entities.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CustomerRepo extends MongoRepository<Customer,Integer> {

}
