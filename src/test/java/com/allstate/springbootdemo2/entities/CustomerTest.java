package com.allstate.springbootdemo2.entities;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CustomerTest {

    @Test
    public void testCustomerConstructorPopulatesField(){
        Customer customer= new Customer(2,"Kim",new Address("l1","l2","l3"));
        assertEquals(2,customer.getId());
    }
}
