package com.allstate.springbootdemo2.dao;

import com.allstate.springbootdemo2.entities.Address;
import com.allstate.springbootdemo2.entities.Customer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest
public class CustomerRepoTest {
    @Autowired
    CustomerRepo customerRepo;

    @Test
    public void test_rowCount(){
        assertTrue(customerRepo.count()>=0);
    }

    @Test
    public void test_findByIdOptionalEmpty(){
        assertEquals(Optional.empty(),customerRepo.findById(99));
    }

    @Test
    public void test_findByIdNotEmpty(){
        assertNotEquals(Optional.empty(),customerRepo.findById(1));
    }

    @Test
    public void test_saveCustomer(){
        Customer customer= new Customer(30,"Tom",new Address("l1","l2","l3"));
        customerRepo.save(customer);
        assertNotEquals(Optional.empty(),customerRepo.findById(30));
    }

}
